/**
 * Copyright (C) 2011 BonitaSoft S.A. BonitaSoft, 32 rue Gustave Eiffel - 38000
 * Grenoble This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2.0 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

$(function() {

	$(document).ready(function() {
		$('#header').updateUI();
	});

});

/**
 * 08/24/2018 - Fix for Chrome 60 compatibility issue from BonitaSoft.
 *
 * From https://community.bonitasoft.com/questions-and-answers/incompatiable-chrome-60
 */
 // Monkey Patch xhr
 // Due to a specification change in the xhr.getAllResponseHeaders method Bonita Portal does not behave as expected
 // in browsers that implement this new specification (currently only Chrome >60).
 // This patch fixes xhr.getAllResponseHeaders unwanted behavior whithin Bonita Portal context
 //    See https://bugs.chromium.org/p/chromium/issues/detail?id=749086
 //    See https://github.com/whatwg/xhr/issues/146
 (function (xhr) {
     var caseSensitiveHeaders = ['Content-Range', 'X-Bonita-API-Token'];

     var getAllResponseHeaders = xhr.getAllResponseHeaders;

     xhr.getAllResponseHeaders = function () {
         var headers = getAllResponseHeaders.apply(this);
         for (var i = 0; i < caseSensitiveHeaders.length; i++) {
             headers = headers.replace(new RegExp('^' + caseSensitiveHeaders[i].toLowerCase(), 'm'), caseSensitiveHeaders[i]);
         }
         return headers;
     }
 })(XMLHttpRequest.prototype)
